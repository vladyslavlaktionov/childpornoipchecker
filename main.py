from concurrent.futures.thread import ThreadPoolExecutor
from concurrent.futures import as_completed
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional, Callable

import requests
from requests import Response
from tqdm import tqdm


@dataclass
class Torrent:
    info_hash: str
    size: int
    name: str


@dataclass
class Content:
    category: str
    imdb_id: int
    name: str
    start_date: datetime
    end_date: datetime
    torrent: Optional[Torrent]
    child_porno: bool


class AntiTorAPIModule:
    api_key: str

    def __init__(self, api_key: str):
        self.api_key = api_key

    def send_request(self, ip_address: str) -> Response:
        endpoint_url = 'https://api.antitor.com/history/peer?ip={ip_address}&days=30&lang=en&key={key}'.format(
            ip_address=ip_address,
            key=self.api_key
        )
        try:
            return requests.get(endpoint_url)
        except requests.exceptions.ConnectionError:
            return self.send_request(ip_address)

    def get_history_of_downloads_by_ip(self, ip_address: str) -> List[Content]:
        content_list: List[Content] = list()
        response = self.send_request(ip_address)
        if response.status_code == 200:
            for content in response.json().get('contents'):
                content_list.append(
                    Content(
                        category=content.get('category'),
                        imdb_id=content.get('imdbId'),
                        name=content.get('name'),
                        start_date=datetime.strptime(content.get('startDate'), "%Y-%m-%dT%H:%M:%S.%f+0000"),
                        end_date=datetime.strptime(content.get('endDate'), "%Y-%m-%dT%H:%M:%S.%f+0000"),
                        child_porno=content.get('childPorno'),
                        torrent=Torrent(
                            info_hash=content.get('torrent').get('infohash'),
                            size=content.get('torrent').get('size'),
                            name=content.get('torrent').get('name'),
                        ) if content.get('torrent') else None
                    )
                )
        return content_list

    def check_if_child_porno(self, ip_address: str) -> bool:
        for content in self.get_history_of_downloads_by_ip(ip_address=ip_address):
            if content.child_porno:
                return True
        return False

    def check_ip_and_save_result(self, ip_address: str, save_result_of_check: Callable) -> None:
        result_of_check = self.check_if_child_porno(ip_address)
        save_result_of_check(ip_address, result_of_check)


if __name__ == "__main__":
    results = open('results.txt', 'a')
    ip_address_list = open('ip_list.txt').read().split('\n')
    API_KEY = "API_KEY"
    anti_tor_api = AntiTorAPIModule(api_key=API_KEY)


    def save_result(ip_address: str, result_of_check: bool):
        if result_of_check:
            results.write(
                '{}\n'.format(ip_address)
            )
        progress_bar.update(1)

    with tqdm(total=len(ip_address_list)) as progress_bar:
        with ThreadPoolExecutor(max_workers=100) as ex:
            futures = [ex.submit(anti_tor_api.check_ip_and_save_result, ip_address, save_result) for ip_address in
                       ip_address_list]
            for future in as_completed(futures):
                result = future.result()
