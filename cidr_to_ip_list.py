import ipaddress

from tqdm import tqdm

cidr_list = open('cidr_list.txt').read().split('\n')

file_with_ip = open('ip_list.txt', 'a')
ip_list = []

for cidr in tqdm(cidr_list):
    ip_list += [str(ip) for ip in ipaddress.IPv4Network(cidr)]

file_with_ip.write("\n".join(ip_list))